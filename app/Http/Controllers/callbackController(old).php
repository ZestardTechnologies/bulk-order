<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use App\ShopModel;
use App\block_config;
use App\Symbol;
use App\ShopCurrency;

class callbackController extends Controller {

    public function index(Request $request) {
        $sh = App::make('ShopifyAPI');
        $app_settings = DB::table('appsettings')->where('id', 1)->first();
        if (!empty($_GET['shop'])) {
            $shop = $_GET['shop'];
            $select_store = DB::table('usersettings')->where('store_name', $shop)->get();

            if (count($select_store) > 0) {
                session(['shop' => $shop]);
                //return redirect()->route('dashboard');
                //Remove coment for the Payment method
                $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $select_store[0]->access_token]);
                $id = $select_store[0]->charge_id;
                $url = 'admin/recurring_application_charges/' . $id . '.json';
                $charge = $sh->call(['URL' => $url, 'METHOD' => 'GET']);
                $charge_id = $select_store[0]->charge_id;
                $charge_status = $select_store[0]->status;
                if (!empty($charge_id) && $charge_id > 0 && $charge_status == "active") {
                    session(['shop' => $shop]);
                    return redirect()->route('dashboard', ['shop' => $shop]);
                } else {
                    return redirect()->route('payment_process', ['shop' => $shop]);
                }
            } else {
                $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop]);

                $permission_url = $sh->installURL(['permissions' => array('read_script_tags', 'write_script_tags', 'read_themes', 'read_products', 'write_products', 'write_content', 'write_themes'), 'redirect' => $app_settings->redirect_url]);
                return redirect($permission_url);
            }
        }
    }

    public function redirect(Request $request) {
        $app_settings = DB::table('appsettings')->where('id', 1)->first();
        if (!empty($request->input('shop')) && !empty($request->input('code'))) {
            $shop = $request->input('shop'); //shop name
            $select_store = DB::table('usersettings')->where('store_name', $shop)->get();
            if (count($select_store) > 0) {
                /* session(['shop' => $shop]);
                  return redirect()->route('dashboard'); */
                //Remove coment for the Payment method
                $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $select_store[0]->access_token]);
                $id = $select_store[0]->charge_id;
                $url = 'admin/recurring_application_charges/' . $id . '.json';
                $charge = $sh->call(['URL' => $url, 'METHOD' => 'GET']);
                $charge_id = $select_store[0]->charge_id;
                $charge_status = $select_store[0]->status;
                if (!empty($charge_id) && $charge_id > 0 && $charge_status == "active") {
                    session(['shop' => $shop]);
                    return redirect()->route('dashboard', ['shop' => $shop]);
                } else {
                    return redirect()->route('payment_process');
                }
            }
            $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop]);
            try {
                $verify = $sh->verifyRequest($request->all());
                if ($verify) {
                    $code = $request->input('code');
                    $accessToken = $sh->getAccessToken($code);
                    //$rand=rand(5,15);
                    //$shop_encrypt=crypt($rand,"shop");
                    DB::table('usersettings')->insert(['access_token' => $accessToken, 'store_name' => $shop, 'store_encrypt' => ""]);
                    $shop_find = ShopModel::where('store_name', $shop)->first();
                    $shop_id = $shop_find->id;

                    $encrypt = crypt($shop_id, "ze");
                    $finaly_encrypt = str_replace(['/', '.'], "Z", $encrypt);

                    DB::table('usersettings')->where('id', $shop_id)->update(['store_encrypt' => $finaly_encrypt]);
                    $shop_find = ShopModel::where('store_name', $shop)->first();

                    $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $shop_find->access_token]);

                    //for creating the uninstall webhook
                    $url = 'https://' . $_GET['shop'] . '/admin/webhooks.json';
                    $webhookData = [
                        'webhook' => [
                            'topic' => 'app/uninstalled',
                            'address' => 'https://zestardshop.com/shopifyapp/bulkorder/uninstall.php',
                            'format' => 'json'
                        ]
                    ];
                    $uninstall = $sh->appUninstallHook($accessToken, $url, $webhookData);

                    //api call for get store info
                    $store_details = $sh->call(['URL' => '/admin/shop.json', 'METHOD' => 'GET']);
                    $shop_currency = new ShopCurrency;
                    $shop_currency->currency_code = $store_details->shop->currency;
                    $shop_currency->shop_id = $shop_id;
                    $shop_currency->save();
                    $currency_format = Symbol::where('currency_code', $store_details->shop->currency)->first();

                    //api call for Create Bulkorder pages

                    $page = $sh->call([
                        'URL' => '/admin/pages.json',
                        'METHOD' => 'GET',
                    ]);
                    foreach ($page->pages as $singlePage) {
                        if ($singlePage->title == 'Bulkorder') {
                            $oldBulkOrderId = $singlePage->id;
                            //echo "<pre/>"; print_r($singlePage);

                            $pageDelete = $sh->call([
                                'URL' => '/admin/pages/' . $oldBulkOrderId . '.json',
                                'METHOD' => 'DELETE',
                            ]);
                        }
                    }

                    $pages = $sh->call([
                        'URL' => '/admin/pages.json',
                        'METHOD' => 'POST',
                        'DATA' => [
                            'page' => [
                                'title' => 'Bulkorder',
                                'body_html' => '<div class="bulkorder" id="' . $shop_find->store_encrypt . '" mony_format="' . $currency_format->symbol_html . '"></div>',
                                'published' => true
                            ]
                        ]
                    ]);

                    //api call for get theme info
                    $theme = $sh->call(['URL' => '/admin/themes/', 'METHOD' => 'GET']);
                    foreach ($theme->themes as $themeData) {
                        if ($themeData->role == 'main') {

                            $theme_id = $themeData->id;
                            $view = (string) View('snippets');

                            //api call for creating snippets
                            $call = $sh->call(['URL' => '/admin/themes/' . $theme_id . '/assets.json', 'METHOD' => 'PUT', 'DATA' => ['asset' => ['key' => 'templates/search.zestardbulkorder-json.liquid', 'value' => $view]]]);
                        }
                    }

                    //api call for creating the app script tag
                    $script = $sh->call(['URL' => '/admin/script_tags.json', 'METHOD' => 'POST', 'DATA' => ['script_tag' => ['event' => 'onload', 'src' => 'https://zestardshop.com/shopifyapp/bulkorder/public/js/bulkorder.js', 'display_scope' => 'online_store']]]);

                    session(['shop' => $shop]);
                    if ($shop == "shopify-dev-app.myshopify.com") {
                        $url = 'https://' . $shop . '/admin/recurring_application_charges.json';
                        $charge = $sh->call([
                            'URL' => $url,
                            'METHOD' => 'POST',
                            'DATA' => array(
                                'recurring_application_charge' => array(
                                    'name' => 'Bulkorder',
                                    'price' => 0.01,
                                    'return_url' => url('payment_success'),
                                    'test' => true
                                )
                            )
                                ], false);
                    } else {
                        //return redirect('dashboard');
                        //creating the Recuring charge for app
                        $url = 'https://' . $shop . '/admin/recurring_application_charges.json';
                        $charge = $sh->call([
                            'URL' => $url,
                            'METHOD' => 'POST',
                            'DATA' => array(
                                'recurring_application_charge' => array(
                                    'name' => 'Bulkorder',
                                    'price' => 3.99,
                                    'return_url' => url('payment_success'),
                                //'test' => true
                                )
                            )
                                ], false);
                    }

                    $create_charge = DB::table('usersettings')->where('store_name', $shop)->update(['charge_id' => (string) $charge->recurring_application_charge->id, 'api_client_id' => $charge->recurring_application_charge->api_client_id, 'price' => $charge->recurring_application_charge->price, 'status' => $charge->recurring_application_charge->status, 'billing_on' => $charge->recurring_application_charge->billing_on, 'payment_created_at' => $charge->recurring_application_charge->created_at, 'activated_on' => $charge->recurring_application_charge->activated_on, 'trial_ends_on' => $charge->recurring_application_charge->trial_ends_on, 'cancelled_on' => $charge->recurring_application_charge->cancelled_on, 'trial_days' => $charge->recurring_application_charge->trial_days, 'decorated_return_url' => $charge->recurring_application_charge->decorated_return_url, 'confirmation_url' => $charge->recurring_application_charge->confirmation_url, 'domain' => $shop]);

                    $shopi_info = $sh->call(['URL' => '/admin/shop.json', 'METHOD' => 'GET']);

                    $headers = 'MIME-Version: 1.0' . "\r\n";
                    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

                    $msg = '<table>
                            <tr>
                                <th>Shop Name</th>
                                <td>' . $shopi_info->shop->name . '</td>
                            </tr>
                            <tr>
                                <th>Email</th>
                                <td>' . $shopi_info->shop->email . '</td>
                            </tr>
                            <tr>
                                <th>Domain</th>
                                <td>' . $shopi_info->shop->domain . '</td>
                            </tr>
                            <tr>
                                <th>Phone</th>
                                <td>' . $shopi_info->shop->phone . '</td>
                            </tr>
                            <tr>
                                <th>Shop Owner</th>
                                <td>' . $shopi_info->shop->shop_owner . '</td>
                            </tr>
                            <tr>
                                <th>Country</th>
                                <td>' . $shopi_info->shop->country_name . '</td>
                            </tr>
                            <tr>
                                <th>Plan</th>
                                <td>' . $shopi_info->shop->plan_name . '</td>
                            </tr>
                          </table>';

                    $store_details = DB::table('development_stores')->get();
                    $development_store = [];
                    foreach ($store_details as $develop_stores) {
                        $development_store[] = $develop_stores->dev_store_name;
                    }
                    if (!in_array($shop, $development_store)) {
                        mail("support@zestard.com", "One Page Bulk Order App Installed", $msg, $headers);
                        mail("chandraprakash.zestard@gmail.com", "One Page Bulk Order App Installed", $msg, $headers);
                        mail("sejal.zestard@gmail.com", "One Page Bulk Order App Installed", $msg, $headers);
                    }
                    //redirecting to the Shopify payment page
                    echo '<script>window.top.location.href="' . $charge->recurring_application_charge->confirmation_url . '"</script>';
                } else {
                    // Issue with data
                }
            } catch (Exception $e) {
                echo '<pre>Error: ' . $e->getMessage() . '</pre>';
            }
        }
    }

    public function dashboard(Request $request) {
        $shop = session('shop');
		if(empty($shop))
		{
			$shop = $_GET['shop'];
			//$shop = $request->input('shop');
			session(['shop' => $shop]);			
		}
        $app_settings = DB::table('appsettings')->where('id', 1)->first();
        $shop_model = new ShopModel;
        $shop_find = ShopModel::where('store_name', $shop)->first();
        $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $shop_find->access_token]);
        $store_details = $sh->call(['URL' => '/admin/shop.json', 'METHOD' => 'GET']);
        $currency_format = Symbol::where('currency_code', $store_details->shop->currency)->first();
        $app_currencyformat = ShopCurrency::where('shop_id', $shop_find->id)->first();
		$new_install = $shop_find->new_install;
        $currencyformat_app = Symbol::where('currency_code', $app_currencyformat->currency_code)->first();
        $all_currency = Symbol::all();
        $shop_domain = $store_details->shop->domain;

        // Update App
      //  if ($shop == "smilotricsdemo.myshopify.com") {
            $url = 'https://' . $shop . '/admin/oauth/access_scopes.json';
            $variant_data = $sh->call([
                'URL' => $url,
                'METHOD' => 'GET'
                    ], false);
            $scope_array = array();
            foreach ($variant_data->access_scopes as $scope_data) {
                $scope_array[] = $scope_data->handle;
            }
            if (!in_array('read_products', $scope_array) && !in_array('write_products', $scope_array)) {
                return view('update_app');
            }
      //  }

        return view('dashboard', ['store_detail' => $currency_format, 'app_currency' => $currencyformat_app, 'currency' => $all_currency, 'shop_details' => $shop_find, 'shop_domain' => $shop_domain, 'new_install' => $new_install ]);
    }

    public function payment_method(Request $request) {
        $shop = session('shop');
        $app_settings = DB::table('appsettings')->where('id', 1)->first();
        $select_store = DB::table('usersettings')->where('store_name', $shop)->get();

        if (count($select_store) > 0) {
            $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $select_store[0]->access_token]);

            $charge_id = $select_store[0]->charge_id;
            $url = 'admin/recurring_application_charges/' . $charge_id . '.json';
            $charge = $sh->call(['URL' => $url, 'METHOD' => 'GET']);
            if (count($charge) > 0) {
                if ($charge->recurring_application_charge->status == "pending") {
                    echo '<script>window.top.location.href="' . $charge->recurring_application_charge->confirmation_url . '"</script>';
                } elseif ($charge->recurring_application_charge->status == "declined" || $charge->recurring_application_charge->status == "expired") {
                    //creating the new Recuring charge after declined app
                    $url = 'https://' . $shop . '/admin/recurring_application_charges.json';
                    $charge = $sh->call([
                        'URL' => $url,
                        'METHOD' => 'POST',
                        'DATA' => array(
                            'recurring_application_charge' => array(
                                'name' => 'Bulkorder',
                                'price' => 3.99,
                                'return_url' => url('payment_success'),
                            //'test' => true
                            )
                        )
                            ], false);

                    $create_charge = DB::table('usersettings')->where('store_name', $shop)->update(['charge_id' => (string) $charge->recurring_application_charge->id, 'api_client_id' => $charge->recurring_application_charge->api_client_id, 'price' => $charge->recurring_application_charge->price, 'status' => $charge->recurring_application_charge->status, 'billing_on' => $charge->recurring_application_charge->billing_on, 'payment_created_at' => $charge->recurring_application_charge->created_at, 'activated_on' => $charge->recurring_application_charge->activated_on, 'trial_ends_on' => $charge->recurring_application_charge->trial_ends_on, 'cancelled_on' => $charge->recurring_application_charge->cancelled_on, 'trial_days' => $charge->recurring_application_charge->trial_days, 'decorated_return_url' => $charge->recurring_application_charge->decorated_return_url, 'confirmation_url' => $charge->recurring_application_charge->confirmation_url, 'domain' => $shop]);

                    //redirecting to the Shopify payment page
                    echo '<script>window.top.location.href="' . $charge->recurring_application_charge->confirmation_url . '"</script>';
                } elseif ($charge->recurring_application_charge->status == "accepted") {

                    $active_url = '/admin/recurring_application_charges/' . $charge_id . '/activate.json';
                    $Activate_charge = $sh->call(['URL' => $active_url, 'METHOD' => 'POST', 'HEADERS' => array('Content-Length: 0')]);
                    $Activatecharge_array = get_object_vars($Activate_charge);
                    $active_status = $Activatecharge_array['recurring_application_charge']->status;
                    $update_charge_status = DB::table('usersettings')->where('store_name', $shop)->where('charge_id', $charge_id)->update(['status' => $active_status]);
                    return redirect()->route('dashboard', ['shop' => $shop]);
                }
            }
        }
    }

    public function payment_compelete(Request $request) {
        $app_settings = DB::table('appsettings')->where('id', 1)->first();
        $shop = session('shop');
        $select_store = DB::table('usersettings')->where('store_name', $shop)->get();

        $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $select_store[0]->access_token]);
        $charge_id = $_GET['charge_id'];
        $url = 'admin/recurring_application_charges/#{' . $charge_id . '}.json';
        $charge = $sh->call(['URL' => $url, 'METHOD' => 'GET',]);
        $status = $charge->recurring_application_charges[0]->status;

        $update_charge_status = DB::table('usersettings')->where('store_name', $shop)->where('charge_id', $charge_id)->update(['status' => $status]);

        if ($status == "accepted") {
            $active_url = '/admin/recurring_application_charges/' . $charge_id . '/activate.json';
            $Activate_charge = $sh->call(['URL' => $active_url, 'METHOD' => 'POST', 'HEADERS' => array('Content-Length: 0')]);
            $Activatecharge_array = get_object_vars($Activate_charge);
            $active_status = $Activatecharge_array['recurring_application_charge']->status;
            $update_charge_status = DB::table('usersettings')->where('store_name', $shop)->where('charge_id', $charge_id)->update(['status' => $active_status]);
            return redirect()->route('dashboard', ['shop' => $shop]);
        } elseif ($status == "declined") {
            echo '<script>window.top.location.href="https://' . $shop . '/admin/apps"</script>';
        }
        //return redirect()->route('dashboard');
    }

    public function Currency(Request $request) {
        $shop = session('shop');
        //dd($request->currency);
        $app_settings = DB::table('appsettings')->where('id', 1)->first();
        $shop_model = new ShopModel;
        $shop_find = ShopModel::where('store_name', $shop)->first();
        $currency_details = Symbol::where('id', $request->currency)->first();
        $currency_update = ShopCurrency::where('shop_id', $shop_find->id)->first();
        //dd($currency_update);
        $currency_update->currency_code = $currency_details->currency_code;
        $currency_update->save();
        $currency_updated = ShopCurrency::where('shop_id', $shop_find->id)->first();

        $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $shop_find->access_token]);
        $store_details = $sh->call(['URL' => '/admin/shop.json', 'METHOD' => 'GET']);
        $currency_format = Symbol::where('currency_code', $store_details->shop->currency)->first();
        $app_currencyformat = ShopCurrency::where('shop_id', $shop_find->id)->first();
        $currencyformat_app = Symbol::where('currency_code', $app_currencyformat->currency_code)->first();
        $all_currency = Symbol::all();
        $new_format = Symbol::where('currency_code', $currency_updated->currency_code)->first();
        return view('dashboard', ['store_detail' => $currency_format, 'app_currency' => $currencyformat_app, 'currency' => $all_currency, 'currency_updated' => $new_format, 'shop_details' => $shop_find]);
    }

    public function validatefrontend(Request $request) {
        $id = $request->id;
        //dd($id);
        
        $shop_find = ShopModel::where('store_encrypt', $id)->first();
        
        if ($shop_find->status == "active") {
            if($shop_find->store_name == "segurico.myshopify.com"){
                //return view('search', ['shop_find' => $shop_find]);
                return view('seguri_search', ['shop_find' => $shop_find]);  
                  
            }
            else{
                return view('search', ['shop_find' => $shop_find]);
            }
            
        } else {
            return "Page Not Found";
        }
    }

    public function get_user_settings(Request $request) {
        $shop = $request['shop'];
        $settings = ShopModel::select('display_product_image_status', 'show_available_quantity', 'additional_css','show_out_of_stock_products')->where('store_name', $shop)->first();
        return $settings;
    }

    public function save_settings(Request $request) {

        $shop = session('shop');
        DB::table('usersettings')->where('store_name', $shop)->update(
		['quantity_status' => $request['quantity_status'], 
		'display_product_image_status' => $request['display_product_image_status'], 
		'additional_css' => $request['additional_css'], 
		'page_title_label' => $request['page_title_label'], 
		'product_name_label' => $request['product_name_label'], 
		'quantity_label' => $request['quantity_label'], 
		'cost_label' => $request['cost_label'], 
		'total_label' => $request['total_label'], 
		'allow_out_of_stock_products_to_order' => isset($request['allow_out_of_stock_products_to_order']) ? '1' : '0', 
		'show_out_of_stock_products' => isset($request['show_out_of_stock_products']) ? '1' : '0', 
		'show_available_quantity' => isset($request['show_available_quantity']) ? '1' : '0', 
		'available_quantity_label' => $request['available_quantity_label']]);
		
		$shop_find = ShopModel::where('store_name' , $shop)->first();
		$shop_find->new_install = 'N';
		$shop_find->save();	
		
        $notification = array(
            'message' => 'Settings Saved Successfully.',
            'alert-type' => 'success'
        );
        return redirect('dashboard')->with('notification', $notification);
    }

    public function get_variant_quantity_by_id(Request $request) 
	{
        $variantId = $_POST['variantId'];
        $sh = App::make('ShopifyAPI');
        $app_settings = DB::table('appsettings')->where('id', 1)->first();

        //$shop = session('shop');
        $shop = $request->input('shop_name');
        $select_store = DB::table('usersettings')->where('store_name', $shop)->get();

        $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $select_store[0]->access_token]);

        $url = 'https://' . $shop . '/admin/variants/' . $variantId . '.json';

        $variant_data = $sh->call([
            'URL' => $url,
            'METHOD' => 'GET'
                ], false);

        echo $variant_data->variant->inventory_quantity;
    }
	public function update_modal_status(Request $request)
	{
		$shop = $request->input('shop_name');
		$shop_find = ShopModel::where('store_name' , $shop)->first();
		$shop_find->new_install = 'N';
		$shop_find->save();		
	}
	
}
